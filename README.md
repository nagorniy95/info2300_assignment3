Author: Artem Nahornyi
URL: crypto.novacorp.pro
Description: NovaCrypto - Website where you can find the latest crypto news, prices, hot deals.
Stack: HTML, CSS, JS, PHP(Back End) 
==================================================================================
API's which I used to get the data:
1) API BITCOIN.COM
- The base URI for all API calls is: https://index-api.bitcoin.com/api
- BTC: https://index-api.bitcoin.com/api/v0/price/usd

2) API COINMARKETCATP.COM
- https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?sort=market_cap&start=0&limit=10&cryptocurrency_type=tokens&convert=USD,BTC

3) API CryptoCompare.com
- https://min-api.cryptocompare.com/data/v2/news/?lang=EN

4) API BitMex
https://www.bitmex.com/api/explorer/
- https://www.bitmex.com/api/v1/orderBook/L2?symbol=XBTUSD&depth=0

5) API BitFinex
- https://api.bitfinex.com/v1/book/btcusd?limit_bids=999999999
- https://api.bitfinex.com/v1/book/btcusd?limit_asks=999999999

==================================================================================

License:

I use "MIT License"

A short and simple permissive license with conditions only requiring preservation of copyright and license notices. Licensed works, modifications, and larger works may be distributed under different terms and without source code.